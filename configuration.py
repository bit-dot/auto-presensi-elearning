import errno
import json
import os
import random
import time
import sys
from configparser import ConfigParser


class PathConfig:
    LOCATION = os.path.join('info.cfg')
    RECONF = ConfigParser()
    RECONF.read(LOCATION)
    JSON_LOC = os.path.join('jadwal.json')
    DRIVER = os.path.join('driver/geckodriver')


UserModel = PathConfig.RECONF


class UserConfig(PathConfig):
    def __init__(self):
        try:
            if not os.path.exists(PathConfig.LOCATION):
                raise FileNotFoundError(errno.ENOENT,
                                        os.strerror(errno.ENOENT),
                                        PathConfig.LOCATION)
        except FileNotFoundError as e:
            sys.exit(e)


class JadwalConfig(PathConfig):
    def scan(self, day):
        try:
            load = open(self.JSON_LOC).read()
            loader = json.loads(load)
        except IOError as e:
            sys.exit(e)
        return loader[day]


class DriverConfig(PathConfig):
    def __init__(self):
        try:
            path = self.DRIVER
            if not os.path.exists(path):
                raise FileNotFoundError(errno.ENOENT,
                                        os.strerror(errno.ENOENT), path)
        except FileNotFoundError as e:
            sys.exit(e)


class Message:
    INVALID_LOGIN = 'Invalid login, please try again'


class Mode:
    DYNAMIC = 'dynamic'
    STATIC = 'static'


class delay:
    def __init__(self, *arg):
        self.arg = arg

    def dynamic(self):
        """
        dynamic time delay
        """
        rnd = random.randint(self.arg[0], self.arg[1])
        time.sleep(rnd)

    def static(self):
        """
        static time delay
        """
        time.sleep(self.arg[0])
